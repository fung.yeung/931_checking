import pyperclip

from mapping import pk_mapping


def gen_checking_sql(target_table):

    primary_key = pk_mapping[target_table]

    sql = f"""with new_dataset_only as (
      select concat({primary_key}) as pk
      from gbl-imt-ve-datalake-demo.00931_vams_raw_asia.V_{target_table}_v1
      except distinct
      select concat({primary_key})
      from gbl-imt-ve-datalake-prod.00504_vams_asia.V_{target_table}
    ), old_dataset_only as (
      select concat({primary_key}) as pk
      from gbl-imt-ve-datalake-prod.00504_vams_asia.V_{target_table} 
      except distinct
      select concat({primary_key})
      from gbl-imt-ve-datalake-demo.00931_vams_raw_asia.V_{target_table}_v1
    )
      select "931_only" as `desc`, *
      FROM new_dataset_only
      union all
      select "504_only", *
      FROM old_dataset_only
      """
    pyperclip.copy(sql)
    # print("sql pasted to clipboard")
    return sql


gen_checking_sql(target_table="R5USERS")
